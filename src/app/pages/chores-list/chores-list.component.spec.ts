import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoresListComponent } from './invoice-payment-provider.component';

describe('InvoicePaymentProviderComponent', () => {
  let component: ChoresListComponent;
  let fixture: ComponentFixture<ChoresListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoresListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
