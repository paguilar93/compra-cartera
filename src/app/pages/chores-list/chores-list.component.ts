import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import {ChoresService} from '../../services/chores/chores.service';
import {ChoreInterface} from '../../services/chores/choreInterface';
import {NotifierComponent} from '../../components/notifier/notifier.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from '../../components/dialogs/dialog.component';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-chores-list',
  templateUrl: './chores-list.component.html',
})
export class ChoresListComponent implements OnInit, AfterViewInit {
  doClean = false;
  searchError: string | null = null;
  chores: ChoreInterface[] = [];
  selectedType: string | null = null;
  headers: string[] = ['id', 'description', 'status'];
  selection = new SelectionModel<ChoreInterface>(true, []);
  dataSource: MatTableDataSource<ChoreInterface>;

  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  @ViewChild(MatSort, {static: false})

  set sort(value: MatSort) {
    this.dataSource.sort = value;
  }

  constructor(private choresService: ChoresService, public fb: FormBuilder, public dialog: MatDialog, private snackBar: MatSnackBar) {
    this.dataSource = new MatTableDataSource(this.chores);
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.choresService.getChores().subscribe(
      (data) => {
        this.chores = data;
        this.dataSource = new MatTableDataSource(this.chores);
      }
    );
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log('filterValue', filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clean(clean: boolean): void {
    this.doClean = clean;
    this.searchError = null;
    this.chores = [];
    this.selection.clear();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.chores.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // tslint:disable-next-line:typedef
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.chores);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ChoreInterface): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row `;
  }
}
