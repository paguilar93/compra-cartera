import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyWalletFilesComponent } from './buy-wallet-files.component';

describe('BuyWalletFilesComponent', () => {
  let component: BuyWalletFilesComponent;
  let fixture: ComponentFixture<BuyWalletFilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyWalletFilesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyWalletFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
