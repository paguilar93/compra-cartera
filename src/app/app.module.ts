import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatChipsModule} from '@angular/material/chips';
import {MatTooltipModule} from '@angular/material/tooltip';
import {CdkTableModule} from '@angular/cdk/table';
import {InterceptorService} from './services/interceptor.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NotifierComponent} from './components/notifier/notifier.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {DialogComponent} from './components/dialogs/dialog.component';
import {MatSortModule} from '@angular/material/sort';
import {ChoresService} from './services/chores/chores.service';
import {ChoresListComponent} from './pages/chores-list/chores-list.component';
import {BuyWalletFilesComponent} from './pages/buy-wallet-files/buy-wallet-files.component';

@NgModule({
  declarations: [
    AppComponent,
    NotifierComponent,
    DialogComponent,
    ChoresListComponent,
    BuyWalletFilesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSelectModule,
    MatDialogModule,
    MatExpansionModule,
    MatListModule,
    FormsModule,
    MatTableModule,
    FlexLayoutModule,
    MatChipsModule,
    MatTooltipModule,
    CdkTableModule,
    MatSnackBarModule,
    MatRadioModule,
    MatAutocompleteModule,
    MaterialFileInputModule,
    MatProgressBarModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatSortModule,
    MatNativeDateModule
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSelectModule,
    MatDialogModule,
    MatExpansionModule,
    MatListModule,
    FormsModule,
    MatTableModule,
    FlexLayoutModule,
    MatChipsModule,
    MatTooltipModule,
    CdkTableModule,
    MatSnackBarModule,
    MatRadioModule,
    MatAutocompleteModule,
    MaterialFileInputModule,
    MatProgressBarModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatSortModule,
    MatNativeDateModule
  ],
  providers: [ChoresService, {
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi: true
  },
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}

