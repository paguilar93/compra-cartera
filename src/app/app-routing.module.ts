import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ChoresListComponent} from './pages/chores-list/chores-list.component';
import {BuyWalletFilesComponent} from './pages/buy-wallet-files/buy-wallet-files.component';

const routes: Routes = [
  {
    path: 'chores-list',
    component: ChoresListComponent
  },
  {
    path: 'buy-wallet-files',
    component: BuyWalletFilesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
